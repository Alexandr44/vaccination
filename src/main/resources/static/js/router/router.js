import Vue from 'vue';
import VueRouter from 'vue-router';
import PatientsListEdit from 'components/PatientsListEdit.vue'
import PatientsList from 'components/PatientsList.vue'

Vue.use(VueRouter);

const routes = [
    { path: '/', component: PatientsList },
    { path: '/patient_edit', component: PatientsListEdit },
    { path: '*', component: PatientsList },
];

export default new VueRouter({
    mode: 'history',
    routes: routes
});

