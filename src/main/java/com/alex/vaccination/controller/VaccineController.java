package com.alex.vaccination.controller;

import com.alex.vaccination.exceptions.NotFoundException;
import com.alex.vaccination.model.Vaccine;
import com.alex.vaccination.repository.PatientRepository;
import com.alex.vaccination.repository.VaccineRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/patient/{patientId}/vaccines")
@NoArgsConstructor
public class VaccineController {

    @Autowired
    private VaccineRepository vaccineRepository;

    @Autowired
    private PatientRepository patientRepository;

    @GetMapping
    public Page<Vaccine> getAll(
            @PathVariable String patientId,
            @PageableDefault(sort = {"vaccineId"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (!patientRepository.existsById(patientId)) throw new NotFoundException("Patient not found", patientId);
        return vaccineRepository.findByPatientId(patientId, pageable);
    }

    @GetMapping("{vaccineId}")
    public Vaccine getOne(@PathVariable String patientId, @PathVariable String vaccineId) {
        if (!patientRepository.existsById(patientId)) throw new NotFoundException("Patient not found", patientId);
        return vaccineRepository.findById(vaccineId)
                .orElseThrow(() -> new NotFoundException("Vaccine not found", vaccineId));
    }

    @PostMapping
    public Vaccine create(@PathVariable String patientId, @Valid @RequestBody Vaccine vaccine) {
        if (!patientRepository.existsById(patientId)) throw new NotFoundException("Patient not found", patientId);
        vaccine.setPatient(patientRepository.findById(patientId)
                .orElseThrow(() -> new NotFoundException("Patient not found", patientId)));
        return vaccineRepository.save(vaccine);
    }

    @PutMapping("{vaccineId}")
    public Vaccine update(@PathVariable String patientId, @PathVariable String vaccineId, @Valid @RequestBody Vaccine vaccineUpd) {
        if (!patientRepository.existsById(patientId)) throw new NotFoundException("Patient not found", patientId);
        final Vaccine vaccine = vaccineRepository.findById(vaccineId)
                .orElseThrow(() -> new NotFoundException("Vaccine not found", vaccineId));
        BeanUtils.copyProperties(vaccineUpd, vaccine, "vaccineId", "patient");
        return vaccineRepository.save(vaccine);
    }

    @DeleteMapping("{vaccineId}")
    public void delete(@PathVariable String patientId, @PathVariable String vaccineId) {
        if (!patientRepository.existsById(patientId)) throw new NotFoundException("Patient not found", patientId);
        final Vaccine vaccine = vaccineRepository.findById(vaccineId)
                .orElseThrow(() -> new NotFoundException("Vaccine not found", vaccineId));
        vaccineRepository.delete(vaccine);
    }

}
