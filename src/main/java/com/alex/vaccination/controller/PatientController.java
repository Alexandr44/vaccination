package com.alex.vaccination.controller;

import com.alex.vaccination.exceptions.NotFoundException;
import com.alex.vaccination.model.Patient;
import com.alex.vaccination.model.Vaccine;
import com.alex.vaccination.repository.PatientRepository;
import com.alex.vaccination.repository.VaccineRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("patient")
@NoArgsConstructor
public class PatientController {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private VaccineRepository vaccineRepository;

    @GetMapping("all")
    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    @GetMapping("all/vaccines")
    public Page<Vaccine> getAllVaccines(
            @PageableDefault(sort = {"vaccineId"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return vaccineRepository.findAll(pageable);
    }

    @GetMapping("{id}")
    public Patient getOne(@PathVariable String id) {
        return patientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Patient not found", id));
    }

    @PostMapping
    public Patient create(@Valid @RequestBody Patient patient) {
        return patientRepository.save(patient);
    }

    @PutMapping("{id}")
    public Patient update(@PathVariable String id, @Valid @RequestBody Patient patientUpd) {
        final Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Patient not found", id));
        BeanUtils.copyProperties(patientUpd, patient, "id");
        return patientRepository.save(patient);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable String id) {
        final Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Patient not found", id));
        patientRepository.delete(patient);
    }

}
