package com.alex.vaccination.model;

import com.alex.vaccination.validators.InsuranceNumberConstraint;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter @Setter
@Table(name = "patients")
@NoArgsConstructor
@ToString @EqualsAndHashCode
public class Patient {

    @Id
    @Column(unique = true, nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotBlank
    @Column(nullable = false)
    private String surname;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Column
    private String patronymic;

    @PastOrPresent
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date birthDate;

    @NotBlank
    @Column(nullable = false)
    private String gender;

    @InsuranceNumberConstraint
    @Column(nullable = false, unique = true)
    private String insuranceNumber;

}

