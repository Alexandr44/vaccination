package com.alex.vaccination.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "vaccines")
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Vaccine {

    @Id
    @Column(unique = true, nullable = false)
    private String vaccineId = UUID.randomUUID().toString();

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private boolean agreement;

    @PastOrPresent
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "patientId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Patient patient;

}
