package com.alex.vaccination.repository;

import com.alex.vaccination.model.Vaccine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccineRepository extends JpaRepository<Vaccine, String> {

    @Override
    Page<Vaccine> findAll(Pageable pageable);

    Page<Vaccine> findByPatientId(String patientId, Pageable pageable);
}
