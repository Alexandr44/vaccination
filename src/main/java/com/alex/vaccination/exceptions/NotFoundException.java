package com.alex.vaccination.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    private String id;

    public NotFoundException(String message, String id) {
        super(message+": "+id);
        this.id = id;
    }



}
