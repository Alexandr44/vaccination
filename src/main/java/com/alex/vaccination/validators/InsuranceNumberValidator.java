package com.alex.vaccination.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InsuranceNumberValidator implements ConstraintValidator<InsuranceNumberConstraint, String> {

    @Override
    public void initialize(InsuranceNumberConstraint constraintAnnotation) {
    }

    @Override
    public boolean isValid(String insuranceNumber, ConstraintValidatorContext constraintValidatorContext) {
        if (insuranceNumber == null || insuranceNumber.isEmpty()) return false;
        if (!insuranceNumber.matches("^\\d{3}-?\\d{3}-?\\d{3}[- ]?\\d{2}$")) return false;

        insuranceNumber = insuranceNumber.replace("-", "").replace(" ","");
        final int lastTwoDigits = Integer.parseInt(insuranceNumber.substring(9));
        int sum = 0, check = 0;

        for (int i=0 ;i<9; i++) {
            int val = Character.getNumericValue(insuranceNumber.charAt(i));
            sum += (9-i)*val;
        }
        if (sum < 100) {
            check = sum;
        }
        else if (sum > 101) {   //  Если сумма = 101, то остаток от деления на 101 будет 0, что мы уже и имеем.
            check = sum%101;
            if (check == 100) {
                check = 0;
            }
        }

        if (check != lastTwoDigits) {
            constraintValidatorContext.buildConstraintViolationWithTemplate("Check sum not valid").addConstraintViolation();
            return false;
        }

        return true;
    }
}
