package com.alex.vaccination.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Target;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Documented
@Constraint(validatedBy = InsuranceNumberValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface InsuranceNumberConstraint {
    String message() default "Invalid insurance number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
